<?php


// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CambioModalidad',
	

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		
		// uncomment the following to enable the Gii tool
		
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'jean123',
		 	// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
	),

	// application components
	'components'=>array(
		
		

       /* 'clientScript' => array(
            'packages' => array(
                'jquery' => array(
                    'baseUrl' =>'/cambiomodalidad/public/js/',
                    'js' => array('jquery.js'),
                    'coreScriptPosition'=>CClientScript::POS_HEAD
                ),
            )
        ),*/

		
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

		//
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'rules'=>array(

					'<controller:\w+>/<id:\d+>'=>'<controller>/view',
					'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
					'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				),

			),

		'db'=>array(
			'connectionString' => 'pgsql:host=localhost;dbname=cambio_modalidad',
			'emulatePrepare' => true,
			'username' => 'postgres',
			'password' => 'jean123',
			'charset' => 'utf8',
		),
		
		'dbsuspension'=>array(
			'class' => 'CDbConnection',
			'connectionString' => 'pgsql:host=localhost;dbname=jubiladb_jean',
			'emulatePrepare' => true,
			'username' => 'postgres',
			'password' => 'jean123',
			'charset' => 'utf8',
		),
/*
		   'finiquitos' => array(
            'class' => 'CDbConnection',
            'connectionString' => 'pgsql:host=' . Db::$hostfini . ';port=' . Db::$portfini. ';dbname=' . Db::$namefini,
            'emulatePrepare' => true,
            'username' => Db::$userfini,
            'password' => Db::$passwordfini,
            'charset' => 'utf8',
            'schemaCachingDuration' => '86400',
            'enableParamLogging' => true,
        ),*/


		'errorHandler'=>array(
			// use 'site/error' action to display errors
            'errorAction'=>'site/error',
        ),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
	'language'=>'es',
);
