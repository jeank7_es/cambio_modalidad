<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
    <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/cambiomodalidad/index.php">CAMBIO MODALIDAD</a>
            </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul id="yw0" class="nav navbar-nav">
            
                    <li>
                        <a class="active" href="/cambiomodalidad/index.php">
                        <i class="fa fa-home"></i> Inicio
                        </a>
                    </li>  
        
                     <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/create">
                        <i class="glyphicon glyphicon-ban-circle"></i> Crear Suspensión
                        </a>
                    </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-alt"></i> Listado de Suspensiones<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/admin">
                         suspensiones y reactivaciones
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         suspendidos
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         reactivados
                        </a>
                        </li>
                    </ul>
                </li>
                    
                    <li>
                        <a class="logout_open" href="<?php echo Yii::app()->request->baseUrl;?>/site/login">
                        <i class="fa fa-user"></i><strong><?php echo ''.Yii::app()->user->name.'';?></strong>  <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
            </ul>
        </nav>
    </div>    
</header>
<div class="container" style="margin-left: 1px; margin-top: 1px">
            
</div>
    
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>
                        Cambio Modalidad
                        <small>Divisón De Nomina</small>
                    </h1>
                </div>
	
                                        <?php 
                                                if(isset($mensaje) && !empty($mensaje)){ 
                                            ?>
                                            <div  class="alert alert-success fade in">

                                            <a href="#" class="close" data-dismiss="alert">&times;</a>

                                            <label class="control-label"><strong>Excelente!</strong> se ha reactivado al funcionario de Forma Exitosa.</label>

                                            </div>
                                        <?php
                                            }?> 

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'tdatos-personal-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        //'id_funcionario',
        'nacionalidad',
        'cedula',
        'nombre1',
        'apellido1',
        'noficio',
        'motivo',
        'f_ingreso',
        /*
        'id_estado',
        'nombre2',
        'apellido2',
        'id_banco',
        'id_estado',
        'id_concepto',
        'quincena_extraordinaria',
        'status',
        */
        array(
            'type' => 'raw',
            'header'=>'Acciones',
            'value'=>array($this,'columnaAcciones'),
        ),
        ),
        
    
)); ?>


<a class="btn btn-danger" role="button" href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/admin">

                <i class="icon-arrow-left bigger-110"></i>
                Volver
</a>
            </div>
		</div>
	</div>	
</div>                            
	