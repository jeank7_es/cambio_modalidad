<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<!-- <div class="row">
		<?php echo $form->label($model,'id_funcionario'); ?>
		<?php echo $form->textField($model,'id_funcionario'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nacionalidad'); ?>
		<?php echo $form->textField($model,'nacionalidad',array('size'=>1,'maxlength'=>1)); ?>
	</div> -->

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Cedula</label>
		<?php echo $form->textField($model,'cedula'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->label($model,'nombre1'); ?>
		<?php echo $form->textField($model,'nombre1',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nombre2'); ?>
		<?php echo $form->textField($model,'nombre2',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido1'); ?>
		<?php echo $form->textField($model,'apellido1',array('size'=>25,'maxlength'=>25)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'apellido2'); ?>
		<?php echo $form->textField($model,'apellido2',array('size'=>25,'maxlength'=>25)); ?>
	</div> -->

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Banco</label>
		<?php echo $form->textField($model,'id_banco'); ?>
	</div>

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Estado</label>
		<?php echo $form->textField($model,'id_estado',array('style' => 'width:50%','class
        ' => 'col-xs-30 col-sm-30')); ?>
	</div>

	<!-- <div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Quincena</label>
		<?php echo $form->textField($model,'id_concepto',array('style' => 'width:50%','size'=>25,'maxlength'=>25,'class
        ' => 'col-xs-30 col-sm-30')); ?>
	</div>

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Quincena extraordinaria</label>
		<?php echo $form->textField($model,'quincena_extraordinaria',array('style' => 'width:50%','size'=>60,'maxlength'=>100,'class
        ' => 'col-xs-30 col-sm-30'));  ?>
            
	</div> -->

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > N° Oficio</label>
		<?php echo $form->textField($model,'noficio',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->label($model,'motivo'); ?>
		<?php echo $form->textField($model,'motivo',array('size'=>60,'maxlength'=>500)); ?>
	</div> -->

	<div class="col-md-4">
		<label  class="col-md-12" for="nombre" > Fecha</label>
		<?php echo $form->textField($model,'f_ingreso'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>1,'maxlength'=>1)); ?>
	</div> -->

	<div class="col-md-4">
		<button class="btn btn-primary btn-next">
                Buscar
                <i class="icon-save"></i>
        </button>	
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->