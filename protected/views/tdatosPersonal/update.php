 <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
    <div class="container">
           <div class="navbar-header">
                <button class="navbar-toggle" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/cambiomodalidad/index.php">CAMBIO MODALIDAD</a>
            </div
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul id="yw0" class="nav navbar-nav">
            
                    <li>
                        <a class="active" href="/cambiomodalidad/index.php">
                        <i class="fa fa-home"></i> Inicio
                        </a>
                    </li>  
        
                     <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/create">
                        <i class="glyphicon glyphicon-ban-circle"></i> Crear Suspensión
                        </a>
                    </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-alt"></i> Listado de Suspensiones<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/admin">
                         suspensiones y reactivaciones
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         suspendidos
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         reactivados
                        </a>
                        </li>
                    </ul>
                </li>
                    
                    <li>
                        <a class="logout_open" href="<?php echo Yii::app()->request->baseUrl;?>/site/login">
                        <i class="fa fa-user"></i><strong><?php echo ''.Yii::app()->user->name.'';?></strong>  <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
            </ul>
        </nav>
    </div>    
</header>
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>
                        Cambio Modalidad
                        <small>Divisón De Nomina</small>
                        
                    </h1>
                </div>



    <ol class="breadcrumb" style="width: 88%; margin-left: 15px">
        <li><i class="fa fa-dashboard"></i>  <a href="/cambiomodalidad/index.php">Inicio</a></li>
        <li class="active">Formulario de Suspension</li>
    </ol>

    <div class="col-lg-11">
        <div class="portlet portlet-blue">
                          <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Crear Suspension</h4>
                                        </div>
                                <div class="portlet-widgets">
                                    <a href="#formControls" data-parent="#accordion" data-toggle="collapse"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                    <div class="clearfix">
                    
                                    </div>
                            </div>


                                  <?php 
                                                if(isset($mensaje) && !empty($mensaje)){ 
                                            ?>
                                        </div>
                         <i class="fa fa-check"></i> <label class="control-label"> Se ha Suspendido el personal de forma exitosa</label>
                                              
                                        <div>
                                            <?php
                                               }?>
    <div class="panel-collapse in" id="formControls" style="height: auto;">
    <div class="portlet-body" style="height:400px;">



<p class="note">Campos con<span class="required">*</span> son requeridos.</p>
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tdatos-personal-form',
	'enableAjaxValidation' => false,
        ));?>
	
	
	
	<?php echo $form->errorSummary($model); ?>
	
			
			<div class="col-md-4">
			<label  class="col-md-12" for="nombre" ><span class="required"> * </span> Nacionalidad</label>
			<?php echo $form->dropDownList($model,'nacionalidad',CHtml::listData(Tnacionalidad::model()->findAll(),'nacionalidad','nacionalidad'),array('empty'=>'Seleccione una nacionalidad','style' => 'width:100%', 'maxlength' => 50, 'required' => 'required'));
        	?>
        	</div>
			

			
        	<div class="col-md-4">
            <label class="col-md-12" for="nombre" ><span class="required"> * </span> Cédula</label>
            <?php
            echo $form->textField($model, 'cedula', array('style' => 'width:100%', 'maxlength' => 8, 'required' => 'required'));
            ?>
            </div>
			
			
			
            <div class="col-md-4">
            <label class="col-md-12" for="nombre"><span class="required"> * </span> Primer nombre</label>
            <?php echo $form->textField($model, 'nombre1', array('style' => 'width:100%', 'maxlength' => 25, 'class' => 'col-xs-30 col-sm-30', 'required' => 'required'));
            ?>
			</div>
 			


			<div class="col-md-4">
       		<label class="col-md-12" for="nombre"> Segundo nombre</label>
        	<?php echo $form->textField($model, 'nombre2', array('style' => 'width:100%', 'maxlength' => 25, 'class
			' => 'col-xs-30 col-sm-30'));
        	?>
    		</div>

    		<div class="col-md-4">
        	<label class="col-md-12" for="nombre"><span class="required"> * </span> Primer apellido </label>
        	<?php echo $form->textField($model, 'apellido1', array('style' => 'width:100%', 'maxlength' => 25, 'class
			' => 'col-xs-30 col-sm-30', 'required' => 'required'));
        	?>
    		</div>

    		<div class="col-md-4">
        	<label class="col-md-12" for="nombre"> Segundo apellido </label>
        	<?php echo $form->textField($model, 'apellido2', array('style' => 'width:100%', 'maxlength' => 25, 'class
			' => 'col-xs-30 col-sm-30'));
        	?>
    		</div>
            
            <div class="col-md-4">
			<label class="col-md-12" for="nombre"><span class="required"> * </span> Banco </label>		
			<?php echo $form->dropDownList($model,'id_banco',CHtml::listData(Tbanco::model()->findAll(),'id_banco','nombre_banco'),array('empty'=>'Seleccione un banco','style' => 'width:100%', 'required' => 'required')); 
			?>
			</div>


			<div class="col-md-4">
			<label class="col-md-12" for="nombre"><span class="required"> * </span> Estado </label>
			<?php echo $form->dropDownList($model,'id_estado',CHtml::listData(Testado::model()->findAll(),'id_estado','nombre_estado'),array('empty'=>'Seleccione un estado','style' => 'width:100%','class
			' => 'col-xs-30 col-sm-30', 'required' => 'required')); 
		 	?>
			</div>

			<div class="col-md-4">
            <label class="col-md-12" for="nombre"><span class="required"> * </span> Quincena </label>
            <?php echo $form->dropDownList($model,'id_concepto',CHtml::listData(Tconceptos::model()->findAll(),'id_concepto','id_concepto'),array('empty'=>'Seleccione una quincena','style' => 'width:100%','class
            ' => 'col-xs-30 col-sm-30', 'required' => 'required')); 
            ?>
            
            </div>

            <div class="col-md-4">
            <label class="col-md-12" for="nombre"><span class="required"> * </span> Quincena extraordinaria </label>
            <?php echo $form->textField($model,'quincena_extraordinaria',array('style' => 'width:100%','size'=>60,'maxlength'=>100,'class
            ' => 'col-xs-30 col-sm-30', 'required' => 'required'));  ?>
            </div>
	

			<div class="col-md-4">
			<label class="col-md-12" for="nombre"><span class="required"> * </span> N° Oficio </label>
			<?php echo $form->textField($model,'noficio',array('style' => 'width:100%','size'=>20,'maxlength'=>20,'class
			' => 'col-xs-30 col-sm-30', 'required' => 'required')); 
			echo $form->error($model,'noficio',array('class' => 'text-error')); 
			?>
			</div>

			<div class="col-md-4">
			<label class="col-md-12" for="nombre"><span class="required"> * </span> Motivo </label>
			<?php  echo $form->textField($model,'motivo',array('style' => 'width:100%','size'=>60,'maxlength'=>500,'class
			' => 'col-xs-30 col-sm-30', 'required' => 'required')); 
		 	?>
			</div>

			<div class="col-md-4">
			<label class="col-md-12" for="nombre"><span class="required"> * </span> Fecha </label>
			 <?php  echo $form->textField($model,'f_ingreso',array('style' => 'width:100%','size'=>60,'maxlength'=>500,'class
			' => 'col-xs-30 col-sm-30', 'required' => 'required')); 
		 	?> 
			</div>

            <div class="row"></div>
            <br>
            <div class="col-md-4">
            
            <!--<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Guardar', array('class' => 'btn btn-primary')); ?>-->
            <a class="btn btn-danger" role="button" href="/cambiomodalidad/tdatosPersonal/admin">

                <i class="icon-arrow-left bigger-110"></i>
                Volver
            </a>

            <button class="btn btn-success btn-next" data-last="Finish" id="boton_suspender">
                Guardar
                <i class="icon-save"></i>
            </button>
			<?php $this->endWidget(); ?>
	       </div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>

<script>
    $(document).ready(function() {

        $("#TdatosPersonal_cedula").on('blur', function() {
            var cedula = $("#TdatosPersonal_cedula").val();
            var nacionalidad = $("#TdatosPersonal_nacionalidad").val();
            //e.preventDefault();
            $.ajax({
                url: '/cambiomodalidad/tdatosPersonal/ObtenerDatos',
                data: {cedula: cedula, nacionalidad: nacionalidad},
                dataType: 'json',
                type: 'POST',
                success: function(resp)
                {
                	if(resp.estatus=='vacio'){
                  	alert("Cedula no se encuentra registrada en el sistema");
                      $("#TdatosPersonal_nombre2").val('');
                      $("#TdatosPersonal_nombre1").val('');
                    $("#TdatosPersonal_apellido1").val('');
                    $("#TdatosPersonal_apellido2").val('');
                    
                	}else{
                    $("#TdatosPersonal_nombre1").val(resp.nomina.primer_nombre);
                    $("#boton_suspender").removeClass('hide');
                    $("#TdatosPersonal_nombre2").val(resp.nomina.segundo_nombre);
                    $("#TdatosPersonal_apellido1").val(resp.nomina.primer_apellido);
                    $("#TdatosPersonal_apellido2").val(resp.nomina.segundo_apellido);


                    
                    $("#respuesta").html(resp);
                }
                }

            });
        });
        $('#TdatosPersonal_f_ingreso').datepicker();

       

        $.datepicker.setDefaults($.datepicker.regional['es']);
        $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        'showOn': 'focus',
        'showOtherMonths': false,
        'selectOtherMonths': true,
        'changeMonth': true,
        'changeYear': true,
        minDate: new Date(1800, 1, 1),
        maxDate: 'today'
    });
    });
</script>
