<?php
$this->breadcrumbs=array(
	'Tdatos Personals',
);

$this->menu=array(
	array('label'=>'Create TdatosPersonal', 'url'=>array('create')),
	array('label'=>'Manage TdatosPersonal', 'url'=>array('admin')),
);
?>

<h1>Tdatos Personals</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
