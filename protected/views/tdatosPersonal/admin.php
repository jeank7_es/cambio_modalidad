<?php
$this->breadcrumbs=array(
	'Tdatos Personals'=>array('index'),
	'Manage',
);



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tdatos-personal-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
    <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/cambiomodalidad/index.php">CAMBIO MODALIDAD</a>
            </div>
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul id="yw0" class="nav navbar-nav">
            
                    <li>
                        <a class="active" href="/cambiomodalidad/index.php">
                        <i class="fa fa-home"></i> Inicio
                        </a>
                    </li>  
        
                     <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/create">
                        <i class="glyphicon glyphicon-ban-circle"></i> Crear Suspensión
                        </a>
                    </li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-alt"></i> Listado de Suspensiones<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/admin">
                         suspensiones y reactivaciones
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         suspendidos
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         reactivados
                        </a>
                        </li>
                    </ul>
                </li>
             </ul>    
                    
                <ul class="nav navbar-nav navbar-right">
        
                    <li>
                        <a class="logout_open" href="<?php echo Yii::app()->request->baseUrl;?>/site/login">
                        <i class="fa fa-user"></i><strong><?php echo ''.Yii::app()->user->name.'';?></strong>  <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                </ul>
           
        </nav>
    </div>    
</header>
<div class="container" style="margin-left: 1px; margin-top: 1px">
            
</div>
    
    <div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>
                        Cambio Modalidad
                        <small>Divisón De Nomina</small>
                    </h1>
                </div>
        <div class="col-lg-11">
        <div class="portlet portlet-blue">
                          <div class="portlet-heading">
                                        <div class="portlet-title">
                                            <h4>Listado de suspensiones</h4>
                                        


                                        </div>
                                <div class="portlet-widgets">
                                    <a href="#formControls" data-parent="#accordion" data-toggle="collapse"><i class="fa fa-chevron-down"></i></a>
                                </div>
                                    <div class="clearfix">
                    
                                    </div>
                            </div>

                                         
    <div class="panel-collapse in" id="formControls" style="height: auto;">
    <div class="portlet-body" style="height:1000px;">


<?php echo CHtml::link('Busqueda avanzada','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tdatos-personal-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'id_funcionario',
		'nacionalidad',
		'cedula',
		'nombre1',
        'id_estado',
		'apellido1',
        'noficio',
        'motivo',
        'f_ingreso',
		/*
		'nombre2',
        'apellido2',
		'id_banco',
		'id_estado',
		'id_concepto',
		'quincena_extraordinaria',
		'status',
		*/
		array(
            'type' => 'raw',
            'header'=>'Acciones',
            'value'=>array($this,'columnaAcciones'),
        ),
        ),
        
    
)); ?>
            <a class="btn btn-danger" role="button" href="/cambiomodalidad/index.php">

                <i class="icon-arrow-left bigger-110"></i>
                Volver
            </a>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</div>