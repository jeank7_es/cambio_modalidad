<?php

$this->pageTitle=Yii::app()->name. "Formulario de registro";

$this->breadcrumbs=array("registro");
?>

<h3>Formulario de Registro</h3>

<div class="form">
	<?php 
	$form=$this->beginWidget('CActiveForm',array(
		'method'=> 'post',
		'action'=> Yii::app()->createUrl('site/registro'),
		'id'=> 'form',
		'enableClientValidation'=>true,
		'enableAjaxValidation'=>true,
		'clientOptions' => array(
		'validateOnSubmit' => true,
		),
	));
	?>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'rol'); 
		echo $form->dropDownList($model,'id_rol',CHtml::listData(Troles::model()->findAll(),'id_rol','rol_descripcion'),array('empty'=>'Seleccione un rol'));
		echo $form->error($model,'id_rol',array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'cedula'); 
		echo $form->textField($model,'cedula'); 
		echo $form->error($model,'cedula', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'nombre'); 
		echo $form->textField($model,'nombre',array('size'=>20,'maxlength'=>20)); 
		echo $form->error($model,'nombre', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'apellido');
		echo $form->textField($model,'apellido',array('size'=>20,'maxlength'=>20));
		echo $form->error($model,'apellido', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'username');
		echo $form->textField($model,'username',array('size'=>60,'maxlength'=>128));
		echo $form->error($model,'username', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'password');
		echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128)); 
		echo $form->error($model,'password', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row">
		<?php 
		echo $form->labelEx($model,'repetir_password');
		echo $form->passwordField($model,'repetir_password',array('size'=>60,'maxlength'=>128)); 
		echo $form->error($model,'repetir_password', array('class' => 'text-error')); 
		?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton("Registrarme", array('class'=> 'btn btn-primary')); 
		?>
	</div>
	<?php $this->endWidget();?>
</div>