 <header class="navbar navbar-inverse navbar-fixed-top bs-docs-nav" role="banner">
    <div class="container">
            <div class="navbar-header">
                <button class="navbar-toggle" data-target=".bs-navbar-collapse" data-toggle="collapse" type="button">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/cambiomodalidad/index.php">CAMBIO MODALIDAD</a>
            </div
        <nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation">
            <ul id="yw0" class="nav navbar-nav">
            
                    <li>
                        <a class="active" href="/cambiomodalidad/index.php">
                        <i class="fa fa-home"></i> Inicio
                        </a>
                    </li>  
        
                     <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/create">
                        <i class="glyphicon glyphicon-ban-circle"></i> Crear Suspensión
                        </a>
                    </li>
                     
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-list-alt"></i> Listado de Suspensiones<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li>
                        <a href="<?php echo Yii::app()->request->baseUrl;?>/tdatosPersonal/admin">
                         suspensiones y reactivaciones
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         suspendidos
                        </a>
                        </li>
                        <li>
                        <a href="#">
                         reactivados
                        </a>
                        </li>
                    </ul>
                </li>
            </ul>     
                <ul class="nav navbar-nav navbar-right">
        
                    <li>
                        <a class="logout_open" href="<?php echo Yii::app()->request->baseUrl;?>/site/login">
                        <i class="fa fa-user"></i><strong><?php echo ''.Yii::app()->user->name.'';?></strong>  <i class="fa fa-sign-out"></i> Salir
                        </a>
                    </li>
                </ul>    
        </nav>
    </div>    
</header>
<div id="page-wrapper">
    <div class="page-content page-content-ease-in">
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>
                        Cambio Modalidad
                        <small>Divisón De Nomina</small>
                        <p>Primera Fase.</p> 
                    </h1>
                
                </div>
            </div>
        </div>
    </div>
</div>

