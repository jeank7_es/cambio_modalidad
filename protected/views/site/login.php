<?php
$this->pageTitle=Yii::app()->name . ' - Login';
$this->breadcrumbs=array(
	'Login',
);
?>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
)); ?>

<body>
<div class="col-md-4 col-md-offset-4">
	 <div class="login-banner text-center">
                    <!--<h1><i class="fa fa-gears"></i> Cambio Modalidad-->
						<img class="img-responsive" alt="" src="<?php echo Yii::app()->request->baseUrl;?>/public/img/OficinadeRecursosHumanos_memos.jpg">								

                    </h1>
                </div>
                <div class="portlet portlet-blue">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Usuario y clave válidos!</strong>
                            </h4>
                        </div>
                        <!--<div class="portlet-widgets">
                            <button class="btn btn-white btn-xs"><i class="fa fa-plus-circle"></i> Nuevo usuario</button>
                        </div>-->
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <form role="form" accept-charset="UTF-8">
                            <fieldset>
                                <div class="form-group">
                                    <input id="LoginForm_username" type="text" name="LoginForm[username]" placeholder="Usuario" class="form-control">
                                    
                                </div>
                                <div class="form-group">
                                    <input id="LoginForm_password" type="password"  name="LoginForm[password]" placeholder="Password" class="form-control">
                                    
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" value="Remember Me" name="remember">Recuerdame
                                    </label>
                                </div>
                                <br>
                                <?php echo CHtml::submitButton('Ingresar', array ('class'=> 'btn btn-lg btn-blue btn-block')); ?>
                            </fieldset>
                            <br>
                            <!-- <p class="small">
                                <a href="#">Olvido Contraseña?</a>
                            </p> -->
                        </form>
                    </div>
                </div>
                <?php $this->endWidget(); ?>
</div>
</body>


