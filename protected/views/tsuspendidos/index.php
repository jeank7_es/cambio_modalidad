<?php
$this->breadcrumbs=array(
	'Tsuspendidoses',
);

$this->menu=array(
	array('label'=>'Create Tsuspendidos', 'url'=>array('create')),
	array('label'=>'Manage Tsuspendidos', 'url'=>array('admin')),
);
?>

<h1>Tsuspendidoses</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
