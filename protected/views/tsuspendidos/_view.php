<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_suspendido')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_suspendido), array('view', 'id'=>$data->id_suspendido)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_concepto')); ?>:</b>
	<?php echo CHtml::encode($data->id_concepto); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_usuario')); ?>:</b>
	<?php echo CHtml::encode($data->id_usuario); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qna_desde')); ?>:</b>
	<?php echo CHtml::encode($data->qna_desde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ano_desde')); ?>:</b>
	<?php echo CHtml::encode($data->ano_desde); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_ingreso')); ?>:</b>
	<?php echo CHtml::encode($data->f_ingreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('noficio')); ?>:</b>
	<?php echo CHtml::encode($data->noficio); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('motivo')); ?>:</b>
	<?php echo CHtml::encode($data->motivo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_funcionario')); ?>:</b>
	<?php echo CHtml::encode($data->id_funcionario); ?>
	<br />

	*/ ?>

</div>