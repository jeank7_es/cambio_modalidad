<?php
$this->breadcrumbs=array(
	'Tsuspendidoses'=>array('index'),
	$model->id_suspendido=>array('view','id'=>$model->id_suspendido),
	'Update',
);

$this->menu=array(
	array('label'=>'List Tsuspendidos', 'url'=>array('index')),
	array('label'=>'Create Tsuspendidos', 'url'=>array('create')),
	array('label'=>'View Tsuspendidos', 'url'=>array('view', 'id'=>$model->id_suspendido)),
	array('label'=>'Manage Tsuspendidos', 'url'=>array('admin')),
);
?>

<h1>Update Tsuspendidos <?php echo $model->id_suspendido; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>