<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_suspendido'); ?>
		<?php echo $form->textField($model,'id_suspendido'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_concepto'); ?>
		<?php echo $form->textField($model,'id_concepto',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_usuario'); ?>
		<?php echo $form->textField($model,'id_usuario',array('size'=>12,'maxlength'=>12)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'qna_desde'); ?>
		<?php echo $form->textField($model,'qna_desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ano_desde'); ?>
		<?php echo $form->textField($model,'ano_desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'f_ingreso'); ?>
		<?php echo $form->textField($model,'f_ingreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'noficio'); ?>
		<?php echo $form->textField($model,'noficio',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'motivo'); ?>
		<?php echo $form->textField($model,'motivo',array('size'=>60,'maxlength'=>500)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_funcionario'); ?>
		<?php echo $form->textField($model,'id_funcionario'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->