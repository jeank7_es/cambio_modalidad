<?php
$this->breadcrumbs=array(
	'Tsuspendidoses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Tsuspendidos', 'url'=>array('index')),
	array('label'=>'Manage Tsuspendidos', 'url'=>array('admin')),
);
?>

<h1>Create Tsuspendidos</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>