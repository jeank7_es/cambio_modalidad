<?php
$this->breadcrumbs=array(
	'Tsuspendidoses'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Tsuspendidos', 'url'=>array('index')),
	array('label'=>'Create Tsuspendidos', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('tsuspendidos-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Tsuspendidoses</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tsuspendidos-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id_suspendido',
		'id_concepto',
		'id_usuario',
		'qna_desde',
		'ano_desde',
		'f_ingreso',
		/*
		'noficio',
		'motivo',
		'id_funcionario',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
