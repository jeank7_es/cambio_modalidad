<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tsuspendidos-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'id_concepto'); ?>
		<?php echo $form->dropDownList($model,'id_concepto',CHtml::listData(Tconceptos::model()->findAll(),'id_concepto','id_concepto'),array('empty'=>'Seleccione un concepto'));
		?>
		<?php echo $form->error($model,'id_concepto'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_usuario'); ?>
		<?php echo $form->textField($model,'id_usuario',array('size'=>12,'maxlength'=>12)); ?>
		<?php echo $form->error($model,'id_usuario'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'qna_desde'); ?>
		<?php echo $form->textField($model,'qna_desde'); ?>
		<?php echo $form->error($model,'qna_desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ano_desde'); ?>
		<?php echo $form->textField($model,'ano_desde'); ?>
		<?php echo $form->error($model,'ano_desde'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'f_ingreso'); ?>
		<?php echo $form->textField($model,'f_ingreso'); ?>
		<?php echo $form->error($model,'f_ingreso'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'noficio'); ?>
		<?php echo $form->textField($model,'noficio',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'noficio'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'motivo'); ?>
		<?php echo $form->textField($model,'motivo',array('size'=>60,'maxlength'=>500)); ?>
		<?php echo $form->error($model,'motivo'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_funcionario'); ?>
		<?php echo $form->textField($model,'id_funcionario'); ?>
		<?php echo $form->error($model,'id_funcionario'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->