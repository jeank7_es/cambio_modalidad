<?php
$this->breadcrumbs=array(
	'Tsuspendidoses'=>array('index'),
	$model->id_suspendido,
);

$this->menu=array(
	array('label'=>'List Tsuspendidos', 'url'=>array('index')),
	array('label'=>'Create Tsuspendidos', 'url'=>array('create')),
	array('label'=>'Update Tsuspendidos', 'url'=>array('update', 'id'=>$model->id_suspendido)),
	array('label'=>'Delete Tsuspendidos', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_suspendido),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tsuspendidos', 'url'=>array('admin')),
);
?>

<h1>View Tsuspendidos #<?php echo $model->id_suspendido; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_suspendido',
		'id_concepto',
		'id_usuario',
		'qna_desde',
		'ano_desde',
		'f_ingreso',
		'noficio',
		'motivo',
		'id_funcionario',
	),
)); ?>
