<html lang="en">
<!-- Mirrored from themes.startbootstrap.com/flex-admin-v1.2/ by HTTrack Website Copier/3.x [XR&CO'2008], Tue, 17 Feb 2015 16:05:39 GMT -->
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" name="viewport">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Cambio Modalidad</title>
    <!-- PACE LOAD BAR PLUGIN - This creates the subtle load bar effect at the top of the page. -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/css_pace/pace.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/demo.css" rel="stylesheet" />
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/css_pace/pace.js"></script>
	
    <!-- GLOBAL STYLES - Include these on every page. -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/fonts.googleapis.com/css3ef8.css?family=Ubuntu:300,400,500,700,300italic,400italic,500italic,700italic" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/fonts.googleapis.com/css5c84.css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/icons/font-awesome/css/font-awesome.min.css" rel="stylesheet" />

    <!-- PAGE LEVEL PLUGIN STYLES -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/messenger/messenger.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/messenger/messenger-theme-flat.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/morris/morris.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins/datatables/datatables.css" rel="stylesheet" />
    <!-- THEME STYLES - Include these on every page. -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/style.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/plugins.css" rel="stylesheet" />

    <!-- THEME DEMO STYLES - Use these styles for reference if needed. Otherwise they can be deleted. -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/demo.css" rel="stylesheet" />

    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->

</head>

<body>

<!-- GLOBAL SCRIPTS -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/popupoverlay/jquery.popupoverlay.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/popupoverlay/defaults.js"></script>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"/>
	<script src="http://code.jquery.com/ui/1.10.1/jquery-ui.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/assets/31fa6451/gridview/jquery.yiigridview.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/assets/31fa6451/listview/jquery.yiilistview.js"></script>
    <!-- Logout Notification jQuery -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/popupoverlay/logout.js" type="text/javascript"></script>
    <!-- HISRC Retina Images -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/hisrc/hisrc.js" type="text/javascript"></script>

    <!-- PAGE LEVEL PLUGIN SCRIPTS -->
    <!-- HubSpot Messenger -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/messenger/messenger.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/messenger/messenger-theme-flat.js" type="text/javascript"></script>
    <!-- Date Range Picker -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/daterangepicker/moment.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/daterangepicker/daterangepicker.js" type="text/javascript"></script>
    <!-- Morris Charts -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/morris/raphael-2.1.0.min.js" type="text/javascript"></script>
    <!-- Flot Charts -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/flot/jquery.flot.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/flot/jquery.flot.resize.js" type="text/javascript"></script>
    <!-- Sparkline Charts -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/sparkline/jquery.sparkline.min.js" type="text/javascript"></script>
    <!-- Moment.js -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/moment/moment.min.js" type="text/javascript"></script>
    <!-- jQuery Vector Map -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/jvectormap/maps/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/demo/map-demo-data.js"></script>
    <!-- Easy Pie Chart -->
    <script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/plugins/easypiechart/jquery.easypiechart.min.js" type="text/javascript"></script>
    <!-- DataTables -->    <!-- THEME SCRIPTS -->
<script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/flex.js" type="text/javascript"></script>
<script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/angular/angular.min.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/angular/controller/usuario.js"></script> 
<script src="<?php echo Yii::app()->request->baseUrl;?>/public/js/angular/app/aplicacion.js"></script> 

<!-- Logout Notification Box -->
           <div id="logout">
        <div class="logout-message">
            
                <i class="fa fa-sign-out text-blue"></i> Desea salir?
            </h3>
            <p>Seleccione "Cerrar sesión"<br> para terminar la sesion actual.</p>
            <ul class="list-inline">
                <li>
                    <a href="<?php echo Yii::app()->request->baseUrl;?>/site/login" class="btn btn-blue">
                        <strong>Cerrar sesion</strong>
                    </a>
                </li>
                <li>
                    <button class="logout_close btn btn-blue">Cancelar</button>
                </li>
            </ul>
        </div>
    </div>
    <!-- /#logout -->
    
<?php echo $content;?>
</body>
</html>




















