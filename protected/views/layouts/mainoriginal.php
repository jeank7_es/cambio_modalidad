<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/boostrap/css/bootstrap.css" rel="stylesheet" />
	<link href="<?php echo Yii::app()->request->baseUrl; ?>/public/boostrap/css/bootstrap.min.css" rel="stylesheet" />
	
	<H2><title><?php echo CHtml::encode($this->pageTitle); ?></title></H2>	
</head>

<body>

	<div class="container" id="page">

		<div id="header">
			<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
		</div><!-- header -->
				
				<div id="mainmenu">
					<?php $this->widget('zii.widgets.CMenu',array('items'=>array(
					array('label'=>'Inicio','icon'=>'home','url'=>array('/site/index')),
					/*array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
					array('label'=>'Contact', 'url'=>array('/site/contact')),*/
					array('label'=>'Suspender', 'url'=>array('/tdatosPersonal/create')),
                    /*array('label'=>'Datos Suspension', 'url'=>array('/tsuspendidos/admin')),*/
					/*array('label'=>'Registrarse', 'url'=>array('/tusuario/create'), 'visible'=>Yii::app()->user->isGuest),*/
					array('label'=>'Iniciar sesión', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
					/*array('label'=>'Panel de control ('.Yii::app()->user->name.')', 'url'=>array('/site/panel'), 'visible'=>!Yii::app()->user->isGuest),*/
					array('label'=>'Cerrar sesión ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
					),
					'htmlOptions'=> array('class' => 'nav navbar-nav'),
					)); ?>
				</div><!-- mainmenu -->
		


	
	<div class="container">
		<div class="page-header">
			<br /><br />
			<?php if(isset($this->breadcrumbs)):?>
			<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			)); ?><!-- breadcrumbs -->
	
			<?php endif?>
		</div>

			<?php echo $content; ?>
	</div>
</div><!-- page -->
</body>
</html>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery.js"></script>


<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery-ui-1.10.3.full.min.js"></script>



