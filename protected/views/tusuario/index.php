<?php
$this->breadcrumbs=array(
	'Tusuarios',
);

$this->menu=array(
	array('label'=>'Create Tusuario', 'url'=>array('create')),
	array('label'=>'Manage Tusuario', 'url'=>array('admin')),
);
?>

<h1>Tusuarios</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
