<?php
$this->breadcrumbs=array(
	'Tusuarios'=>array('index'),
	$model->id_usuario,
);

$this->menu=array(
	array('label'=>'List Tusuario', 'url'=>array('index')),
	array('label'=>'Create Tusuario', 'url'=>array('create')),
	array('label'=>'Update Tusuario', 'url'=>array('update', 'id'=>$model->id_usuario)),
	array('label'=>'Delete Tusuario', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_usuario),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Tusuario', 'url'=>array('admin')),
);
?>

<h1>View Tusuario #<?php echo $model->id_usuario; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_usuario',
		'id_rol',
		'cedula',
		'nombre',
		'apellido',
		'username',
		'password',
	),
)); ?>
