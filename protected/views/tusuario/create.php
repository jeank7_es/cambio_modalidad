<?php
$this->breadcrumbs=array(
	'Tusuarios'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'lista de usuarios', 'url'=>array('index')),
	array('label'=>'manejador de usuarios', 'url'=>array('admin')),
);
?>

<h3>Formulario de Registro</h3>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>