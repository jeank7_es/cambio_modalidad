<?php
$this->breadcrumbs=array(
	'Tusuarios'=>array('index'),
	$model->id_usuario=>array('view','id'=>$model->id_usuario),
	'Update',
);

$this->menu=array(
	array('label'=>'Lista de usuarios', 'url'=>array('index')),
	array('label'=>'Crear usuario', 'url'=>array('create')),
	array('label'=>'Tusuario', 'url'=>array('view', 'id'=>$model->id_usuario)),
	array('label'=>'manejador de usuarios', 'url'=>array('admin')),
);
?>

<h1>Modificar usuario <?php echo $model->id_usuario; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>