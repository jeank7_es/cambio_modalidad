<?php

/**
 * This is the model class for table "ttipo_persona".
 *
 * The followings are the available columns in table 'ttipo_persona':
 * @property integer $id_persona
 * @property string $tipo_persona
 *
 * The followings are the available model relations:
 * @property TdatosPersonal[] $tdatosPersonals
 */
class TtipoPersona extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TtipoPersona the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ttipo_persona';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_persona', 'required'),
			array('id_persona', 'numerical', 'integerOnly'=>true),
			array('tipo_persona', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_persona, tipo_persona', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tdatosPersonals' => array(self::HAS_MANY, 'TdatosPersonal', 'id_persona'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_persona' => 'Id Persona',
			'tipo_persona' => 'Tipo Persona',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_persona',$this->id_persona);
		$criteria->compare('tipo_persona',$this->tipo_persona,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}