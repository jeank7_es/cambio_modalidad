<?php


class ValidarRegistro extends CFormModel
{
	public $id_rol;
	public $cedula;
	public $nombre;
	public $apellido;
	public $username;
	public $password;
	public $repetir_password;

	public function rules()
	{
		return array(
			array('username, password,cedula,nombre,apellido,repetir_password', 'required', 'message' => 'Este campo es requerido'),
			array(
				'username',
				'match',
				'pattern' => '/^[a-z0-9\_]+$/i',
				'message' => 'Error, solo letras y guiones bajos',
			),	
			array(
				'password',
				'match',
				'pattern' => '/^[a-z0-9]+$/i',
				'message' => 'Error, solo letras y numeros',
			),
			array(
				'cedula',
				'match',
				'pattern' => '/^[0-9\_]+$/i',
				'message' => 'Error, solo numeros',
			),	
			array(
				'nombre',
				'match',
				'pattern' => '/^[a-z\_]+$/i',
				'message' => 'Error, solo letras',
			),
			array(
				'apellido',
				'match',
				'pattern' => '/^[a-z\_]+$/i',
				'message' => 'Error, solo letras',
			),			
			array(
				'repetir_password',
				'compare',
				'compareAttribute' => 'password',
				'message' => 'El password no coincide'
			),	
			array('cedula', 'unique'),
			array('cedula', 'length', 'max'=>8),
			array('id_rol, cedula', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>20),
			array('username, password,repetir_password', 'length', 'max'=>128),
			

			);

	}
	/*public function comprobar_nombre ($attributes, $params)
	{
		$conexion = Yii::app()->db;	

		$consulta = "Select username from tusuario where";
		$consulta .= "username='".$this->username."'";
		$resultado= $conexion->createCommand($consulta);
		$filas = $resultado->query() ;
		
		foreach ($filas as $fila)
		{
			if ($this->username === $fila ['username'])
			{
				$this->addError('username','Usuario no disponible');
				
				break;
			}
		}
	}
	*/
}
