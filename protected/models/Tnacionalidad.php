<?php

/**
 * This is the model class for table "tnacionalidad".
 *
 * The followings are the available columns in table 'tnacionalidad':
 * @property string $nacionalidad
 * @property string $descripcion
 *
 * The followings are the available model relations:
 * @property TdatosPersonal[] $tdatosPersonals
 */
class Tnacionalidad extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tnacionalidad the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tnacionalidad';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nacionalidad', 'required'),
			array('nacionalidad', 'length', 'max'=>1),
			array('descripcion', 'length', 'max'=>12),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('nacionalidad, descripcion', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tdatosPersonals' => array(self::HAS_MANY, 'TdatosPersonal', 'nacionalidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'nacionalidad' => 'Nacionalidad',
			'descripcion' => 'Descripcion',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('descripcion',$this->descripcion,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}