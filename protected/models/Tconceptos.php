<?php

/**
 * This is the model class for table "tconceptos".
 *
 * The followings are the available columns in table 'tconceptos':
 * @property string $id_concepto
 * @property string $concepto_descripcion
 * @property integer $numero_qna
 *
 * The followings are the available model relations:
 * @property Tsuspendidos[] $tsuspendidoses
 */
class Tconceptos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tconceptos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tconceptos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_concepto', 'required'),
			array('numero_qna', 'numerical', 'integerOnly'=>true),
			array('id_concepto', 'length', 'max'=>4),
			array('concepto_descripcion', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_concepto, concepto_descripcion, numero_qna', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tsuspendidoses' => array(self::HAS_MANY, 'Tsuspendidos', 'id_concepto'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_concepto' => 'Id Concepto',
			'concepto_descripcion' => 'Concepto Descripcion',
			'numero_qna' => 'Numero Qna',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_concepto',$this->id_concepto,true);
		$criteria->compare('concepto_descripcion',$this->concepto_descripcion,true);
		$criteria->compare('numero_qna',$this->numero_qna);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}