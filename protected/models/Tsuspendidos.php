<?php

/**
 * This is the model class for table "tsuspendidos".
 *
 * The followings are the available columns in table 'tsuspendidos':
 * @property integer $id_suspendido
 * @property string $id_concepto
 * @property string $id_usuario
 * @property integer $qna_desde
 * @property integer $ano_desde
 * @property string $f_ingreso
 * @property string $noficio
 * @property string $motivo
 * @property integer $id_funcionario
 *
 * The followings are the available model relations:
 * @property Tconceptos $idConcepto
 * @property Tusuario $idUsuario
 */
class Tsuspendidos extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tsuspendidos the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tsuspendidos';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('qna_desde, ano_desde, id_funcionario', 'numerical', 'integerOnly'=>true),
			array('id_concepto', 'length', 'max'=>4),
			array('id_usuario', 'length', 'max'=>12),
			array('noficio', 'length', 'max'=>20),
			array('motivo', 'length', 'max'=>500),
			array('f_ingreso', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_suspendido, id_concepto, id_usuario, qna_desde, ano_desde, f_ingreso, noficio, motivo, id_funcionario', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idConcepto' => array(self::BELONGS_TO, 'Tconceptos', 'id_concepto'),
			'idUsuario' => array(self::BELONGS_TO, 'Tusuario', 'id_usuario'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_suspendido' => 'Id Suspendido',
			'id_concepto' => 'Id Concepto',
			'id_usuario' => 'Id Usuario',
			'qna_desde' => 'Qna Desde',
			'ano_desde' => 'Ano Desde',
			'f_ingreso' => 'F Ingreso',
			'noficio' => 'Noficio',
			'motivo' => 'Motivo',
			'id_funcionario' => 'Id Funcionario',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_suspendido',$this->id_suspendido);
		$criteria->compare('id_concepto',$this->id_concepto,true);
		$criteria->compare('id_usuario',$this->id_usuario,true);
		$criteria->compare('qna_desde',$this->qna_desde);
		$criteria->compare('ano_desde',$this->ano_desde);
		$criteria->compare('f_ingreso',$this->f_ingreso,true);
		$criteria->compare('noficio',$this->noficio,true);
		$criteria->compare('motivo',$this->motivo,true);
		$criteria->compare('id_funcionario',$this->id_funcionario);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}