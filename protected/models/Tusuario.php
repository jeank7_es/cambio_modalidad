<?php

/**
 * This is the model class for table "tusuario".
 *
 * The followings are the available columns in table 'tusuario':
 * @property integer $id_usuario
 * @property integer $id_rol
 * @property integer $cedula
 * @property string $nombre
 * @property string $apellido
 * @property string $username
 * @property string $password
 *
 * The followings are the available model relations:
 * @property Troles $idRol
 */
class Tusuario extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tusuario the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tusuario';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password,cedula,nombre,apellido', 'required','message' => 'Este campo es requerido'),
			array(
				'username',
				'match',
				'pattern' => '/^[a-z0-9\_]+$/i',
				'message' => 'Error, solo letras y guiones bajos',
			),	
			
			array(
				'cedula',
				'match',
				'pattern' => '/^[0-9\_]+$/i',
				'message' => 'Error, solo numeros',
			),	
			array(
				'nombre',
				'match',
				'pattern' => '/^[a-z\_]+$/i',
				'message' => 'Error, solo letras',
			),
			array(
				'apellido',
				'match',
				'pattern' => '/^[a-z\_]+$/i',
				'message' => 'Error, solo letras',
			),			
			array('username,cedula', 'unique'),
			array('cedula', 'length', 'max'=>8),
			array('id_rol, cedula', 'numerical', 'integerOnly'=>true),
			array('nombre, apellido', 'length', 'max'=>20),
			array('username, password', 'length', 'max'=>128),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_usuario, id_rol, cedula, nombre, apellido, username, password', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idRol' => array(self::BELONGS_TO, 'Troles', 'id_rol'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_usuario' => 'Id Usuario',
			'id_rol' => 'Id Rol',
			'cedula' => 'Cedula',
			'nombre' => 'Nombre',
			'apellido' => 'Apellido',
			'username' => 'Username',
			'password' => 'Password',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_usuario',$this->id_usuario);
		$criteria->compare('id_rol',$this->id_rol);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre',$this->nombre,true);
		$criteria->compare('apellido',$this->apellido,true);
		$criteria->compare('Username',$this->username,true);
		$criteria->compare('password',$this->password,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	protected function beforevalidate(){

		if($this->isNewRecord)
		{	
			$t_hasher = new PasswordHash(8, TRUE);
			$hash = $t_hasher->HashPassword($this->password);
			$this->password = $hash;

	}
	return parent::beforevalidate();

	}
	protected function afterValidate (){
		if($this->isNewRecord)
		{
			if(count($this-> getErrors())>0)
			{

				$this->password = "";
			}	

	}
	return parent::afterValidate();

	}
	public function validarPassword($passwd)
	{			
		$t_hasher = new PasswordHash(8, TRUE);
		return $t_hasher->checkPassword($passwd, $this->password);


	}
	
}