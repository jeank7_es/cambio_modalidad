<?php

/**
 * This is the model class for table "tdatos_personal".
 *
 * The followings are the available columns in table 'tdatos_personal':
 * @property integer $id_funcionario
 * @property string $nacionalidad
 * @property integer $cedula
 * @property string $nombre1
 * @property string $nombre2
 * @property string $apellido1
 * @property string $apellido2
 * @property integer $id_banco
 * @property integer $id_estado
 * @property string $id_concepto
 * @property string $quincena_extraordinaria
 * @property string $noficio
 * @property string $motivo
 * @property string $f_ingreso
 * @property string $status
 *
 * The followings are the available model relations:
 * @property Tbanco $idBanco
 * @property Testado $idEstado
 * @property Tnacionalidad $nacionalidad0
 */
class TdatosPersonal extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return TdatosPersonal the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tdatos_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cedula, id_banco, id_estado', 'numerical', 'integerOnly'=>true),
			array('nacionalidad, status', 'length', 'max'=>1),
			array('nombre1, nombre2, apellido1, apellido2, id_concepto', 'length', 'max'=>25),
			array('quincena_extraordinaria', 'length', 'max'=>100),
			array('noficio', 'length', 'max'=>20),
			array('motivo', 'length', 'max'=>500),
			array('f_ingreso', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_funcionario, nacionalidad, cedula, nombre1, nombre2, apellido1, apellido2, id_banco, id_estado, id_concepto, quincena_extraordinaria, noficio, motivo, f_ingreso, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idBanco' => array(self::BELONGS_TO, 'Tbanco', 'id_banco'),
			'idEstado' => array(self::BELONGS_TO, 'Testado', 'id_estado'),
			'nacionalidad0' => array(self::BELONGS_TO, 'Tnacionalidad', 'nacionalidad'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_funcionario' => 'Id Funcionario',
			'nacionalidad' => 'Nacionalidad',
			'cedula' => 'Cedula',
			'nombre1' => 'Nombre1',
			'nombre2' => 'Nombre2',
			'apellido1' => 'Apellido1',
			'apellido2' => 'Apellido2',
			'id_banco' => 'Id Banco',
			'id_estado' => 'Id Estado',
			'id_concepto' => 'Id Concepto',
			'quincena_extraordinaria' => 'Quincena Extraordinaria',
			'noficio' => 'Noficio',
			'motivo' => 'Motivo',
			'f_ingreso' => 'F Ingreso',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_funcionario',$this->id_funcionario);
		$criteria->compare('nacionalidad',$this->nacionalidad,true);
		$criteria->compare('cedula',$this->cedula);
		$criteria->compare('nombre1',$this->nombre1,true);
		$criteria->compare('nombre2',$this->nombre2,true);
		$criteria->compare('apellido1',$this->apellido1,true);
		$criteria->compare('apellido2',$this->apellido2,true);
		$criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('id_estado',$this->id_estado);
		$criteria->compare('id_concepto',$this->id_concepto,true);
		$criteria->compare('quincena_extraordinaria',$this->quincena_extraordinaria,true);
		$criteria->compare('noficio',$this->noficio,true);
		$criteria->compare('motivo',$this->motivo,true);
		//$criteria->compare('f_ingreso',$this->f_ingreso,true);
		$criteria->compare('status',$this->status,true);
		
		$criteria->addSearchCondition("TO_CHAR(t.f_ingreso, 'YYYY-MM-DD')", $this->f_ingreso, false, 'AND', '=');

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
	public function datos_activa ($cedula, $nacionalidad)
	{
		$sql = "Select trim(dnombre1) as primer_nombre,trim(dnombre2) as segundo_nombre,trim(dapellido1) as primer_apellido,trim(dapellido2) as segundo_apellido from tnombres_act where ccedula=$cedula and tnacionalidad='$nacionalidad'
				union select trim(dnombre1) as primer_nombre,trim(dnombre2) as segundo_nombre,trim(dapellido1) as primer_apellido,trim(dapellido2) as segundo_apellido from tdatos_personales_sol where cedula_j=$cedula and nacionalidad='$nacionalidad'
		";
		$consulta= Yii::app()->dbsuspension->createCommand($sql);
		$resultado=$consulta->queryRow();
		return $resultado;

	}
}