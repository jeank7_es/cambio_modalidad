<?php

/**
 * This is the model class for table "tbanco".
 *
 * The followings are the available columns in table 'tbanco':
 * @property integer $id_banco
 * @property string $nombre_banco
 * @property string $codigo_banco
 *
 * The followings are the available model relations:
 * @property TdatosPersonal[] $tdatosPersonals
 */
class Tbanco extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return Tbanco the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'tbanco';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_banco', 'required'),
			array('id_banco', 'numerical', 'integerOnly'=>true),
			array('nombre_banco', 'length', 'max'=>20),
			array('codigo_banco', 'length', 'max'=>4),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_banco, nombre_banco, codigo_banco', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tdatosPersonals' => array(self::HAS_MANY, 'TdatosPersonal', 'id_banco'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_banco' => 'Id Banco',
			'nombre_banco' => 'Nombre Banco',
			'codigo_banco' => 'Codigo Banco',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_banco',$this->id_banco);
		$criteria->compare('nombre_banco',$this->nombre_banco,true);
		$criteria->compare('codigo_banco',$this->codigo_banco,true);

		return new CActiveDataProvider(get_class($this), array(
			'criteria'=>$criteria,
		));
	}
}