<?php

class TdatosPersonalController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rulesclass="errorSummary"
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(''),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('view','create','update','obtenerDatos','activar','suspender','vista','vistasus'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete','obtenerDatos','activar','suspender','vista','vistasus'),
				'users'=>array('admin', 'jcarrillo'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new TdatosPersonal;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TdatosPersonal']))
		{
			
			//var_dump($model->f_ingreso); die();
			$model->attributes=$_POST['TdatosPersonal'];
			//$model->f_ingreso=date('Y-m-d',strtotime($_POST['TdatosPersonal']['f_ingreso']));
			$model->status = "S";
			if($model->save()){
				$mensaje="Se ha Suspendido al funcionario de Forma Exitosa";
				$model=new TdatosPersonal();
                                    
                    //$this->registerLog('ESCRITURA', 'catalogo.mencion.activar', 'EXITOSO', 'Se ha activado una Mencion');
                   $this->render("_form", array('mensaje'=>$mensaje,'model'=>$model));

			}
				//$this->redirect(array('view','id'=>$model->id_funcionario));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['TdatosPersonal']))
		{
			$model->attributes=$_POST['TdatosPersonal'];
			if($model->save())

				$this->redirect(array('view','id'=>$model->id_funcionario));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

		if($id)
		{
			//var_dump("hhh"); die();
			// we only allow deletion via POST request
			$this->loadModel($id)->delete();

			// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			if(!isset($_GET['ajax']))
				//var_dump('holaaaaaaa'); die();
				$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
		}
		else
			throw new CHttpException(400,'Invalid request. Please do not repeat this request again.');
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('TdatosPersonal');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

 public function columnaAcciones($datas) {
        $estatus = $datas["status"];
        $id = $datas['id_funcionario'];
        if ($estatus == "S") {
            $columna = CHtml::link("", "/cambiomodalidad/tdatosPersonal/view/". $id, array("class" => "glyphicon glyphicon-hand-up", "title" => "Verificar registro")) .'&nbsp';
            $columna .= CHtml::link("", "/cambiomodalidad/tdatosPersonal/update/".$id, array("class" => "glyphicon glyphicon-refresh", "title" => "Modificar funcionario")).'&nbsp';
            $columna .= CHtml::link("", "/cambiomodalidad/tdatosPersonal/activar/id/".$id, array("class" => "glyphicon glyphicon-saved", "title" => "Reactivar")).'&nbsp';
        } else {
        	$columna = CHtml::link("", "/cambiomodalidad/tdatosPersonal/view/id/".$id, array("class" => "glyphicon glyphicon-hand-up", "title" => "Verificar funcionario")) . '&nbsp';
        	$columna .= CHtml::link("", "/cambiomodalidad/tdatosPersonal/update/".$id, array("class" => "glyphicon glyphicon-refresh", "title" => "Modificar funcionario")).'&nbsp';
            $columna .= CHtml::link("", "/cambiomodalidad/tdatosPersonal/suspender/".$id, array("class" => "glyphicon glyphicon-ban-circle", "title" => "Suspender nuevamente al funcionario")) . '&nbsp;&nbsp';
           	
        }
        return $columna;
    }

    public function actionActivar() {
    	//var_dump("aquii"); die();
    	$mensaje='';
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];

            //$id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                //$model->usuario_act_id = Yii::app()->user->id;
                //$model->fecha_elim = date("Y-m-d H:i:s");
                $model->status = "R";
                if ($model->save()) {
                   
                   $model=new TdatosPersonal('search');
                   
                	//var_dump("Exito"); die();
                	$mensaje="Se ha reactivado al funcionario de Forma Exitosa";
                                     
                    //$this->registerLog('ESCRITURA', 'catalogo.mencion.activar', 'EXITOSO', 'Se ha activado una Mencion');
                   $this->render("vista", array('mensaje'=>$mensaje,'model'=>$model));
                    //$model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
        //$this->render('borrar',array('model'=>$model,));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if(!isset($_GET['ajax']))
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

	public function actionSuspender() {
    	//var_dump("aquii"); die();
    	$mensaje='';
        if (isset($_REQUEST['id'])) {
            $id = $_REQUEST['id'];

            //$id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                //$model->usuario_act_id = Yii::app()->user->id;
                //$model->fecha_elim = date("Y-m-d H:i:s");
                $model->status = "S";
                if ($model->save()) {

                   $model=new TdatosPersonal('search');
                	//var_dump("Exito"); die();
                	$mensaje="Se ha Suspendido al funcionario de Forma Exitosa";
                                     
                    //$this->registerLog('ESCRITURA', 'catalogo.mencion.activar', 'EXITOSO', 'Se ha activado una Mencion');
                   $this->render("vistasus", array('mensaje'=>$mensaje,'model'=>$model));
                    //$model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
        //$this->render('borrar',array('model'=>$model,));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if(!isset($_GET['ajax']))
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    public function actionVista()
	{
		if(Yii::app()->user->isGuest)
		{

			$this->redirect(Yii::app()->homeUrl);

		}
		else
		{
			$this->render('vista');
		}
	}

	public function actionVistasus()
	{
		if(Yii::app()->user->isGuest)
		{

			$this->redirect(Yii::app()->homeUrl);

		}
		else
		{
			$model->status = "S";
			$this->render('vistasus');
		}
	}




	public function actionObtenerDatos(){
		$model =new TdatosPersonal();
		$cedula=$_POST['cedula'];
		$nacionalidad=$_POST['nacionalidad'];
		$nomina=TdatosPersonal::model()->datos_activa($cedula,$nacionalidad);
		if(empty($nomina)){
			echo json_encode(array('nomina'=>$nomina,'estatus'=>'vacio'));

		}else{
	   echo json_encode(array('nomina'=>$nomina,'estatus'=>'success'));

		}
	}



	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new TdatosPersonal('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['TdatosPersonal']))
			$model->attributes=$_GET['TdatosPersonal'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}





	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=TdatosPersonal::model()->findByPk((int)$id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='tdatos-personal-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
}