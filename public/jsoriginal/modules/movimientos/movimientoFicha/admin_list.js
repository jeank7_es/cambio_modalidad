/**
 * Created by nelson on 24/08/15.
 */

$(document).ready(function(){
    $('#MovimientoFicha_departamentoEntregaNombre').bind('keyup', function () {
        keyAlphaNum(this, true, true);
        makeUpper(this);
    });
    $('#MovimientoFicha_departamentoEntregaNombre').bind('blur', function () {
        clearField(this);
    });
    $('#MovimientoFicha_personalCedula').bind('keyup blur', function () {
        keyNum(this, false);
    });
    $('#MovimientoFicha_personalCedula').bind('blur', function () {
        clearField(this);
    });
    $('#cedula').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula').bind('blur', function () {
        clearField(this);
    });
    $('#cedula_funcionario_m').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula_funcionario_m').bind('blur', function () {
        clearField(this);
    });
    $('#cedula_funcionario').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula_funcionario').bind('blur', function () {
        clearField(this);
    });

    $('#MovimientoFicha_diasPrestado').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#MovimientoFicha_diasPrestado').bind('blur', function () {
        clearField(this);
    });


    $("#buscar_expediente").on('click', function(e) {
        e.preventDefault();
        validarExistenciaDeExpediente(e);
    });
    $("#cedula").on('blur', function(e) {
        e.preventDefault();
        validarExistenciaDeExpediente(e);
    });
});

function prueba(id,cedula,nacionalidad){
    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
    console.log(id);
    console.log(cedula);
    console.log(nacionalidad);
}
/*function recibirMovimiento(id, cedula, nacionalidad){
    $(document).ready(function() {
        console.log(id);
        $.ajax({
            url: '/movimientos/MovimientoFicha/recibir',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'html',
            type: 'POST',
            success: function (resp) {
                $("#dialogo_recibir_solicitud").html(resp);
                //$("#dialogo_recibir_funcionario").html(resp);
                recibir(id, cedula, nacionalidad);
            }
        });
    });
}*/
/*function recibirMovimiento(id, cedula, nacionalidad){
    $(document).ready(function() {
        console.log(id);
        $.ajax({
            url: '/movimientos/MovimientoFicha/recibir',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'json',
            type: 'POST',
            success: function (resp,resp1) {
                // $('#movimiento_fecha_solicitud_s').val('09-07-2015');
                console.log(resp);
                var fecha_solicitud="'"+resp.fecha_solicitud+"'";
                $('#movimiento_fecha_solicitud_s').val(resp.fecha_solicitud);
                console.log($('#movimiento_fecha_solicitud_s').val());
                $('#fecha_entrega_act_l').datepicker({minDate: $('#movimiento_fecha_solicitud_s').val(), maxDate: 0});
                //$("#dialogo_recibir_funcionario").html(resp);

            }
        });
        recibir(id, cedula, nacionalidad);
    });
}*/
function recibirMovimiento(id, cedula, nacionalidad){
    /*$("#dialogo_recibir_solicitud").text("");
     $("#dialogo_recibir_solicitud").remove();
     $("#dialogo_recibir_solicitud").removeAttr();*/
    $(document).ready(function() {
        console.log(id);
        $.ajax({
            url: '/movimientos/MovimientoFicha/recibir',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'json',
            type: 'POST',
            success: function (resp,resp1) {
                // $('#movimiento_fecha_solicitud_s').val('09-07-2015');
                console.log(resp);
                var fecha_solicitud="'"+resp.fecha_solicitud+"'";
                $('#movimiento_fecha_solicitud_s').val(resp.fecha_solicitud);
                console.log($('#movimiento_fecha_solicitud_s').val());
                //$('#fecha_entrega_act_l').datepicker({minDate: $('#movimiento_fecha_solicitud_s').val(), maxDate: 0});
                //$('#fecha_entrega_act_l').datepicker({maxDate: 0});
                //$("#dialogo_recibir_funcionario").html(resp);

            }
        });
        //$('#movimiento_fecha_solicitud_s').val('09-07-2015');

        recibir(id, cedula, nacionalidad);
    });
}
function recibir(id, cedula, nacionalidad) {
    $(document).ready(function() {
        $("#dialogo_recibir").removeClass('hide');
        $("#dialogo_recibir").dialog({
            width: 400,
            height: 200,
            show: "scale",
            hide: "scale",
            resizable: "false",
            position: "center",
            modal: "true",
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + 'Devolver Documento' + "</h4></div>",
            title_html: true,

            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    //"class": "btn btn-xs",
                    "class": "btn btn-xs-8",
                    click: function() {
                        $(this).dialog("close");
                        //return false;
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Recibir",
                    "class": "btn btn-danger",

                    click: function() {
                        var fecha_entrega_fin = $("#fecha_entrega_act_l").val();
                        var departamento_id = $("#departamento_id").val();
                        //Loading.show();
                        //console.log('entreeeeeeeee');
                        console.log(fecha_entrega_fin);
                        console.log(departamento_id);
                        $.ajax({
                            url: '/movimientos/movimientoFicha/actualizarFecha',
                            data: {id: id, fecha_entrega_fin: fecha_entrega_fin, cedula: cedula, nacionalidad: nacionalidad, departamento_id: departamento_id},
                            dataType: 'json',
                            type: 'POST',
                            success: function(resp)
                            {
                                $("#exito_html").removeClass('hide');
                                $("#result_DialogBox").removeClass('hide');
                                $("#movimientos_mm").removeClass('hide');
                                //console.log('hola');
                                if(resp.resultado==='exito'){
                                    $("#result_DialogBox").removeClass('errorDialogBox');
                                    $("#result_DialogBox").addClass('successDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_fecha_exito);
                                    //if(cedula===$('#cedula').val()){
                                        //$("#movimientos_mm").removeClass('hide');
                                        //$("#boton_mostrar").removeClass('hide');
                                    //}
                                }else if(resp.resultado==='error_fecha'){
                                    $("#result_DialogBox").removeClass('successDialogBox');
                                    $("#result_DialogBox").addClass('errorDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_fecha);
                                }else if(resp.resultado==='error_departamento'){
                                    $("#result_DialogBox").removeClass('successDialogBox');
                                    $("#result_DialogBox").addClass('errorDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_departamento);
                                }


                                //$("#exito_html").html(resp2.resultado);
                                $("#exito").removeClass('hide');
                                //console.log('------------------------>');
                                //console.log(resp.resultado);
                                //console.log(cedula,nacionalidad);

                                //var personalNombre = 'Richard Jose';
                                $('#movimiento-ficha-grid').yiiGridView('update', {
                                    //data: '?MovimientoFicha[personalCedula]='+cedula+'&MovimientoFicha[personalNacionalidad]='+nacionalidad
                                    //data: '?MovimientoFicha[personalCedula]='+cedula+'&MovimientoFicha[personalNacionalidad]='+nacionalidad
                                });
                            }
                        });
                        //Loading.hide();
                        //$("#dialogo_recibir_solicitud").html('');
                        $(this).dialog("close");
                        //return false;
                    }
                }
            ]
        });
    });
}




function vista(id, cedula, nacionalidad) {
    $(document).ready(function() {
        $.ajax({
            url: '/movimientos/MovimientoFicha/vista',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'html',
            type: 'POST',
            success: function(resp)
            {
                $("#dialogo_recibir_funcionario").removeClass('hide').dialog({
                    width: 850,
                    height: 'auto',
                    show: "scale",
                    hide: "scale",
                    resizable: "false",
                    position: "center",
                    modal: "true",
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + 'Vista del Personal' + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                            "class": "btn btn-xs",
                            click: function() {
                                $(this).dialog("close");
                                //return false;
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Recibir",
                            "class": "hide",
                            click: function() {
                                Loading.show();
                                $(this).dialog("close");
                                //return false;
                            }
                        }
                    ]
                });
                $("#dialogo_recibir_funcionario").html(resp);
            }
        });

    });
}

$(document).ready(function(){
    $('#fecha_entrega_act_l').datepicker({maxDate: 0});
    /**********************************************************/
    $('#date-picker-CGridView').datepicker();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    //var $fechaIni='2015-09-02';
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: "dd-mm-yy",
        showOn:"focus",
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: "Cerrar",
        prevText: "<-Ant",
        nextText: "Sig->",
        currentText: "Hoy",
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene","Feb","Mar","Abr", "May","Jun","Jul","Ago","Sep", "Oct","Nov","Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesShort: ["Dom","Lun","Mar","Mié","Juv","Vie","Sáb"],
        dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
        weekHeader: "Sm"
        //minDate: $fechaIni
    });

});

