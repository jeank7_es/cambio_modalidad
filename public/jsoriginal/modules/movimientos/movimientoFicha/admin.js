/**
 * Created by nelson on 24/08/15.
 */

$(document).ready(function(){
    $('#MovimientoFicha_departamentoEntregaNombre').bind('keyup', function () {
        keyAlphaNum(this, true, true);
        makeUpper(this);
    });
    $('#MovimientoFicha_departamentoEntregaNombre').bind('blur', function () {
        clearField(this);
    });
    $('#MovimientoFicha_personalCedula').bind('keyup blur', function () {
        keyNum(this, false);
    });
    $('#MovimientoFicha_personalCedula').bind('blur', function () {
        clearField(this);
    });
    $('#cedula').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula').bind('blur', function () {
        clearField(this);
    });
    $('#cedula_funcionario_m').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula_funcionario_m').bind('blur', function () {
        clearField(this);
    });
    $('#cedula_funcionario').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#cedula_funcionario').bind('blur', function () {
        clearField(this);
    });

    $('#MovimientoFicha_diasPrestado').bind('keyup blur', function () {
        keyNum(this, false);
        //console.log($('#cedula').val());
    });
    $('#MovimientoFicha_diasPrestado').bind('blur', function () {
        clearField(this);
    });

    $("#buscar_expediente").on('click', function(e) {
        e.preventDefault();
        validarExistenciaDeExpediente(e);
    });
    $("#cedula").on('blur', function(e) {
        e.preventDefault();
        validarExistenciaDeExpediente(e);
    });

    //$('telefono_personal').mask('(0000) 000-00-00');
});

function prueba(id,cedula,nacionalidad){
    console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
    console.log(id);
    console.log(cedula);
    console.log(nacionalidad);
}

function validarExistenciaDeExpediente(e){
    var cedula = $('#cedula').val();
    var nacionalidad = $('#nacionalidad').val();
    //var funcionario_id_r = $("#funcionario_id_r").val();
    //var funcionario_id_r = 1;
    Loading.show();
    //var clas = $('#movimiento-ficha-grid').serialize();
    //var data = $('#movimiento-ficha-grid').serializeObject();
    //console.log(clas);
    $('#movimiento-ficha-grid').yiiGridView('update', {
        // MovimientoFicha[personalCedula]
        data: '?MovimientoFicha[personalCedula]='+cedula+'&MovimientoFicha[personalNacionalidad]='+nacionalidad
        //data:'?MovimientoFicha[personalCedula]='+cedula
        //var data = $('input', row).serializeObject();
        //data: data+'&MovimientoFicha[personalCedula]='+cedula
//                        'acc-payment-recei-grid',
//                        {data: ajaxRequest}

        //data: $('#movimiento_personal').serialize()
    });

    $.ajax({
        url: '/movimientos/movimientoFicha',
        data: {cedula: cedula, nacionalidad: nacionalidad},
        dataType: 'html',
        type: 'POST',
        success: function(resp, resp1, resp3)
        {

            try {
                var json = jQuery.parseJSON(resp3.responseText);

                if (json.statusCode === 'mostrar_boton'){
                    $("#mensaje_saime").addClass('hide');
                    $("#cedula-vacia").addClass('hide');
                    $("#boton_mostrar").removeClass('hide');
                    $("#personal_id_r").val(json.personal_id);
                    //console.log(json.personal_id);
                    //var id = $("#personal_id_r").val();
                    //console.log(id);
                    Loading.hide();
                }else if(json.statusCode === 'ocultar_boton'){
                    $("#mensaje_saime").addClass('hide');
                    $("#cedula-vacia").addClass('hide');
                    $("#boton_mostrar").addClass('hide');
                    $("#personal_id_r").val(json.personal_id);
                    //console.log(json.personal_id);
                    //var id = $("#personal_id_r").val();
                    //console.log(id);
                    Loading.hide();
                }

                if (json.statusCode === 'personal_vacio') {
                    $("#boton_mostrar").addClass('hide');
                    $("#mensaje_saime").addClass('hide');
                    $("#cedula-vacia").addClass('hide');
                    $("#exito_html").addClass('hide');
                    //console.log(json.saime.primer_nombre);
                    $("#nombre_unico").val(json.saime.primer_nombre + ' ' + json.saime.segundo_nombre);
                    $("#apellido_unico").val(json.saime.primer_apellido + ' ' + json.saime.segundo_apellido);
                    $("#cedula_id_unica").val(json.saime.cedula);
                    $("#fecha_nacimiento_unico").val(json.saime.fecha_nacimiento);
                    $("#sexo_unico").val(json.saime.sexo);
                    //$("#dialogo").html('<div class="infoDialogBox"> <p> Registrar Movimiento de la persona </p> </div>');
                    $("#dialogo").removeClass('hide');
                    $("#dialogo").dialog({
                        width: 800,
                        height: 'auto',
                        show: "scale",
                        hide: "scale",
                        resizable: "false",
                        position: "center",
                        modal: "true",
                        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + 'Movimientos' + "</h4></div>",
                        title_html: true,
                        buttons: [
                            {
                                html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cerrar",
                                "class": "btn btn-danger",
                                click: function() {
                                    $(this).dialog("close");
                                }
                            },
                            {
                                html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar Movimiento",
                                "class": "hide",
                                click: function() {

                                }
                            }
                        ]

                    });
                    Loading.hide();
                }
                if (json.statusCode === 'vacio') {
                    $("#mensaje_saime").addClass('hide');
                    $("#cedula-vacia").removeClass('hide');
                    $("#cedula-vacia").html(json.mensaje);
                    Loading.hide();
                }
                if (json.statusCode === 'saime') {
                    $("#cedula-vacia").addClass('hide');
                    $("#mensaje_saime").removeClass('hide');
                    //
                    $("#exito_html").addClass('hide');
                    $("#cedula-vacia").addClass('hide');
                    //
                    $("#mensaje_saime").html(json.mensaje);
                    Loading.hide();
                }

            } catch (e) {

                //$("#unico").addClass('hide');
                $("#mensaje_saime").addClass('hide');
                $("#cedula-vacia").addClass('hide');
                $("#exito_html").removeClass('hide');
                //$("#unico").html('');
                //$("#unico").html(resp);
                //  $("#exito_html").html(resp);
                //$("dialogo_r" ).empty();
                Loading.hide();
            }
        }

    });
}
function recibir(id, cedula, nacionalidad) {
    $(document).ready(function() {
        //console.log('XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX');
        $("#dialogo_recibir").removeClass('hide').dialog({
            width: 400,
            height: 200,
            show: "scale",
            hide: "scale",
            resizable: "false",
            position: "center",
            modal: "true",
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + 'Devolver Documento' + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    //"class": "btn btn-xs",
                    "class": "btn btn-xs-8",
                    click: function() {
                        $(this).dialog("close");
                        //return false;
                    }
                },
                {
                    html: "<i class='icon-save bigger-110'></i>&nbsp; Recibir",
                    "class": "btn btn-danger",
                    click: function() {
                        var fecha_entrega_fin = $("#fecha_entrega_act_l").val();
                        var departamento_id = $("#departamento_id").val();
                        Loading.show();
                        $.ajax({
                            url: '/movimientos/movimientoFicha/actualizarFecha',
                            data: {id: id, fecha_entrega_fin: fecha_entrega_fin, cedula: cedula, nacionalidad: nacionalidad, departamento_id: departamento_id},
                            dataType: 'json',
                            type: 'POST',
                            success: function(resp)
                            {

                                $("#exito_html").removeClass('hide');
                                $("#result_DialogBox").removeClass('hide');
                                $("#movimientos_mm").removeClass('hide');
                                console.log('hola');
                                if(resp.resultado==='exito'){
                                    $("#result_DialogBox").removeClass('errorDialogBox');
                                    $("#result_DialogBox").addClass('successDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_fecha_exito);
                                    if(cedula===$('#cedula').val()){
                                        $("#movimientos_mm").removeClass('hide');
                                        $("#boton_mostrar").removeClass('hide');
                                    }
                                }else if(resp.resultado==='error_fecha'){
                                    $("#result_DialogBox").removeClass('successDialogBox');
                                    $("#result_DialogBox").addClass('errorDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_fecha);
                                }else if(resp.resultado==='error_departamento'){
                                    $("#result_DialogBox").removeClass('successDialogBox');
                                    $("#result_DialogBox").addClass('errorDialogBox');
                                    $("#text_result_DialogBox").html(resp.mensaje_departamento);
                                }


                                //$("#exito_html").html(resp2.resultado);
                                //$("#exito").removeClass('hide');
                                console.log('------------------------>');
                                console.log(resp.resultado);
                                console.log(cedula,nacionalidad);
                                Loading.hide();
                                //var personalNombre = 'Richard Jose';
                                $('#movimiento-ficha-grid').yiiGridView('update', {
                                    data: '?MovimientoFicha[personalCedula]='+cedula+'&MovimientoFicha[personalNacionalidad]='+nacionalidad
                                });

                            }
                        });
                        //$("#dialogo_recibir").html('');

                        $(this).dialog("close");
                        //return false;
                    }
                }
            ]
        });
    });
}

function recibirMovimiento(id, cedula, nacionalidad){
    /*$("#dialogo_recibir_solicitud").text("");
    $("#dialogo_recibir_solicitud").remove();
    $("#dialogo_recibir_solicitud").removeAttr();*/
    $(document).ready(function() {
        console.log(id);
        $.ajax({
            url: '/movimientos/MovimientoFicha/recibir',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'json',
            type: 'POST',
            success: function (resp,resp1) {
               // $('#movimiento_fecha_solicitud_s').val('09-07-2015');
                console.log(resp);
                var fecha_solicitud="'"+resp.fecha_solicitud+"'";
                $('#movimiento_fecha_solicitud_s').val(resp.fecha_solicitud);
                console.log($('#movimiento_fecha_solicitud_s').val());
                //$('#fecha_entrega_act_l').datepicker({minDate: $('#movimiento_fecha_solicitud_s').val(), maxDate: 0});
                //$('#fecha_entrega_act_l').datepicker({maxDate: 0});
                //$("#dialogo_recibir_funcionario").html(resp);

            }
        });
        //$('#movimiento_fecha_solicitud_s').val('09-07-2015');

        recibir(id, cedula, nacionalidad);
    });
}

function vista(id, cedula, nacionalidad) {
    $(document).ready(function() {
        $.ajax({
            url: '/movimientos/MovimientoFicha/vista',
            data: {id: id, cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'html',
            type: 'POST',
            success: function(resp)
            {
                $("#dialogo_recibir_funcionario").removeClass('hide').dialog({
                    width: 850,
                    height: 'auto',
                    show: "scale",
                    hide: "scale",
                    resizable: "false",
                    position: "center",
                    modal: "true",
                    title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + 'Vista del Personal' + "</h4></div>",
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                            "class": "btn btn-xs",
                            click: function() {
                                $(this).dialog("close");
                                //return false;
                            }
                        },
                        {
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Recibir",
                            "class": "hide",
                            click: function() {
                                Loading.show();
                                $(this).dialog("close");
                                //return false;
                            }
                        }
                    ]
                });
                $("#dialogo_recibir_funcionario").html(resp);
            }
        });

    });
}

$(document).ready(function(){
    $("#cedula_funcionario_m").bind('blur', function() {
        var cedula = $("#cedula_funcionario_m").val();
        var nacionalidad = $("#nacionalidad_funcionario_m").val();
        //e.preventDefault();
        //Se mostraran todos aquellos funcionarios existentes por cedula
        $.ajax({
            url: '/movimientos/movimientoFicha/buscarFuncionario',
            data: {cedula: cedula, nacionalidad: nacionalidad},
            dataType: 'json',
            type: 'POST',
            success: function(resp)
            {
                if (resp.statuscode === 'funcionario_encontrado') {
                    $("#mensaje_no_encotrado_m").addClass('hide');
                    $("#boton_movimiento_personal_no_encontrado_l").addClass('hide');
                    $("#boton_movimiento_personal_l").removeClass('hide');
                    $("#funcionario_id_r").val(resp.funcionario.id);
                    $("#nombre_funcionario_m").val(resp.funcionario.nombre);
                    $("#apellido_funcionario_m").val(resp.funcionario.apellido);
                    $("#correo_funcionario_m").val(resp.funcionario.correo);
                    $("#telefono_funcionario_m").val(resp.funcionario.telefono);
                    $("#respuesta").html(resp);
                    $.ajax({
                        url: '/movimientos/movimientoFicha/listaDepartamentoDeFuncionario',
                        data: {funcionario_id_r: resp.funcionario.id,funcionario_cedula: resp.funcionario.cedula, funcionario_nacionalidad: resp.funcionario.nacionalidad},
                        dataType: 'html',
                        type: 'GET',
                        success:function(resp1){
                            console.log(resp1);
                            $("#departamento_solicitud_ll").html(resp1);
                        }
                    });
                } else if ((resp.statuscode === 'funcionario_no_encontrado') || (resp.statuscode === 'cedula_o_nacionaliada_no_definida')) {
                    $("#nombre_funcionario_m").val('');
                    $("#apellido_funcionario_m").val('');
                    $("#correo_funcionario_m").val('');
                    $("#telefono_funcionario_m").val('');
                    $("#mensaje_no_encotrado_m").removeClass('hide');
                    $("#mensaje_no_encotrado_m").html(resp.mensaje);
                    $("#departamento_solicitud_ll").html('');
                    $("#boton_movimiento_personal_l").addClass('hide');
                    $("#boton_movimiento_personal_no_encontrado_l").removeClass('hide');
                }else{
                    console.log(resp);
                }
            }

        });
    });

    $("#guardar_movimientos_l_l").bind('click', function() {
        var id = $("#personal_id_r").html();
        var personal_id = $("#personal_id_r").val();
        var nombre = $("#Personal_movimientos_nombre_r").val();
        var apellido = $("#Personal_movimientos_apellido_r").val();
        var cedula = $("#Personal_movimientos_cedula_r").val();
        var fecha_nacimiento = $("#Personal_movimientos_fecha_nacimiento_r").val();
        var descripcion = $("#descripcion_movimiento_m").val();
        var personal_departamento = $("#departamento_solicitud_ll").val();
        var nacionalidad = $("#nacionalidad").val();
        var sexo = $("#Personal_movimientos_sexo").val();
        var cedula_funcionario = $("#cedula_funcionario_m").val();
        var nacionalidad_funcionario = $("#nacionalidad_funcionario_m").val();
        var funcionario_id_r = $("#funcionario_id_r").val();
        console.log('createExistente----2');
        console.log(descripcion);
        Loading.show();
        $.ajax({
            url: '/movimientos/movimientoFicha/createExistente',
            data: {nombre: nombre, apellido: apellido, cedula: cedula, fecha_nacimiento: fecha_nacimiento, descripcion: descripcion, personal_departamento: personal_departamento, nacionalidad: nacionalidad, sexo: sexo, id: id, personal_id: personal_id, cedula_funcionario: cedula_funcionario,nacionalidad_funcionario: nacionalidad_funcionario, funcionario_id: funcionario_id_r},
            dataType: 'json',
            type: 'POST',
            success: function(resp)
            {
                //console.log(json.saime.primer_nombre);
                console.log(resp);
                //$("#exito_html").html(resp);
                $("#exito_registro_html").removeClass('hide');
                $("#result_registro_DialogBox").removeClass('hide');
                if(resp.resultado==='exito'){
                    $("#result_registro_DialogBox").addClass('successDialogBox');
                    $("#text_result_registro_DialogBox").html(resp.mensaje);
                    $("#boton_mostrar").addClass('hide');
                    $("#boton_mostrar_mm").addClass('hide');
                }else if(resp.resultado==='error_guardar'){
                    $("#result_registro_DialogBox").addClass('errorDialogBox');
                    $("#text_result_registro_DialogBox").html(resp.mensaje);
                }else if(resp.resultado==='error_funcionario'){
                    $("#result_registro_DialogBox").addClass('errorDialogBox');
                    $("#text_result_registro_DialogBox").html(resp.mensaje);
                }

                //vaciar campos despues de crear
                $("#nombre_funcionario_m").val('');
                $("#apellido_funcionario_m").val('');
                $("#correo_funcionario_m").val('');
                $("#telefono_funcionario_m").val('');
                $("#departamento_solicitud_ll").val('');
                $("#descripcion_movimiento_m").val('');

                //$("#exito_registro_html").html(resp.mensaje);
                //$("#crear_movimientos").html(resp);
                Loading.hide();

                $('#movimiento-ficha-grid').yiiGridView('update', {
                    // MovimientoFicha[personalCedula]
                    //data:'?MovimientoFicha[personalCedula]='+cedula
                    data: '?MovimientoFicha[personalCedula]='+cedula+'&MovimientoFicha[personalNacionalidad]='+nacionalidad
                    //data: $('#movimiento_personal').serialize()
                });
                //window.location.href = '/movimientos/personal/cedula/'+cedula+'/nacionalidad_s/'+nacionalidad;
            }
        });
        $("#dialogo_r").addClass('hide').dialog("close");
    });

    //var min='2015-08-02';
    //var min='02-08-2015';
    //var fecha_min= $('#movimiento_fecha_solicitud_s').val();
    //console.log(min);
    //$('#fecha_entrega_act_l').datepicker({minDate: $('#movimiento_fecha_solicitud_s').val(), maxDate: 0});
    //$('#fecha_entrega_act_l').datepicker();
    $('#fecha_entrega_act_l').datepicker({maxDate: 0});
    /**********************************************************/
    $('#date-picker-CGridView').datepicker();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    var fecha = new Date();
    var anoActual = fecha.getFullYear();
    //var $fechaIni='2015-09-02';
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: "dd-mm-yy",
        showOn:"focus",
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        closeText: "Cerrar",
        prevText: "<-Ant",
        nextText: "Sig->",
        currentText: "Hoy",
        monthNames: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
        monthNamesShort: ["Ene","Feb","Mar","Abr", "May","Jun","Jul","Ago","Sep", "Oct","Nov","Dic"],
        dayNames: ["Domingo", "Lunes", "Martes", "Miércoles", "Jueves", "Viernes", "Sábado"],
        dayNamesShort: ["Dom","Lun","Mar","Mié","Juv","Vie","Sáb"],
        dayNamesMin: ["Do","Lu","Ma","Mi","Ju","Vi","Sá"],
        weekHeader: "Sm"
        //minDate: $fechaIni
    });


});

